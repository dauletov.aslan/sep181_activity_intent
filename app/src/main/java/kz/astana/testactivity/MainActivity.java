package kz.astana.testactivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Hello", "onCreate()");
        setContentView(R.layout.activity_main);

        TextView tv = findViewById(R.id.textView);
        Button first = findViewById(R.id.firstButton);
        Button second = findViewById(R.id.secondButton);
        Button send = findViewById(R.id.sendButton);

        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FirstActivity.class);
                intent.putExtra("TEXT_EXTRA", "I'm first");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("A_EXTRA", "I'm second");
                startActivity(intent);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://google.com");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Невозможно открыть ссылку", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Hello", "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Hello", "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Hello", "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Hello", "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "onDestroy()");
    }
}